package ua.pti.myatm;

/**
 * Created by ezalor on 24.11.2014.
 */
public class NotEnoughMoneyInATM extends Throwable {
    @Override
    public void printStackTrace() {
        super.printStackTrace();
        System.err.println("Not enough money in ATM.");
    }
}
