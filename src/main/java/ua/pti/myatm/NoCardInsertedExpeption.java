package ua.pti.myatm;

/**
 * Created by ezalor on 24.11.2014.
 */
public class NoCardInsertedExpeption extends Throwable {
    @Override
    public void printStackTrace() {
        super.printStackTrace();
        System.err.println("No Card Is Inserted");
    }
}
