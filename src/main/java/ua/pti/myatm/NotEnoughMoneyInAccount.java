package ua.pti.myatm;

/**
 * Created by ezalor on 24.11.2014.
 */
public class NotEnoughMoneyInAccount extends Throwable {
    @Override
    public void printStackTrace() {
        super.printStackTrace();
        System.err.println("No enough money to complete operation");
    }
}
