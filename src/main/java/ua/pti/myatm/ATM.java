package ua.pti.myatm;

public class ATM {
    private double moneyInATM;
    private Card insertedCard;
    private boolean isCardValid;

    //Можно задавать количество денег в банкомате 
    ATM(double moneyInATM){
        this.moneyInATM = moneyInATM;
        isCardValid = false;
    }

    // Возвращает каоличестов денег в банкомате
    public double getMoneyInATM(){
        return moneyInATM;
    }
        
    //С вызова данного метода начинается работа с картой
    //Метод принимает карту и пин-код, проверяет пин-код карты и не заблокирована ли она
    //Если неправильный пин-код или карточка заблокирована, возвращаем false. При этом, вызов всех последующих методов у ATM с данной картой должен генерировать исключение NoCardInserted
    public boolean validateCard(Card card, int pinCode){
        insertedCard = card;
          isCardValid = !card.isBlocked() && card.checkPin(pinCode);
        return isCardValid;
    }
    
    //Возвращает сколько денег есть на счету
    public double checkBalance() throws NoCardInsertedExpeption{
        if(isCardValid){
            return insertedCard.getAccount().getBalance();
        }
        else
        {
            throw new NoCardInsertedExpeption();
        }
    }
    
    //Метод для снятия указанной суммы
    //Метод возвращает сумму, которая у клиента осталась на счету после снятия
    //Кроме проверки счета, метод так же должен проверять достаточно ли денег в самом банкомате
    //Если недостаточно денег на счете, то должно генерироваться исключение NotEnoughMoneyInAccount 
    //Если недостаточно денег в банкомате, то должно генерироваться исключение NotEnoughMoneyInATM 
    //При успешном снятии денег, указанная сумма должна списываться со счета, и в банкомате должно уменьшаться количество денег
    public double getCash(double amount) throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
         if(moneyInATM < amount)
                 throw new NotEnoughMoneyInATM();

        else
            if( this.getCashFromAccount(amount)){
                moneyInATM -= amount;
            }
        return this.insertedCard.getAccount().getBalance();
    }

    private boolean getCashFromAccount(double amount) throws NotEnoughMoneyInAccount {
        if(insertedCard.getAccount().getBalance() < amount){

                throw new NotEnoughMoneyInAccount();

        }
        else
        {
         this.insertedCard.getAccount().withdrow(amount);
        }
        return true;
    }
}
