package ua.pti.myatm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;


/**
 * Created by ezalor on 24.11.2014.
 */

public class ATMRunner {

    private ATM atm;
    private ATM emptyATM;
    private Account account;
    private Account emptyAccount;
    private Card card;
    private Card badCard1;
    private Card badCard2;

    static class TestAccount implements Account{
        private int cashOnAccount;

        TestAccount(int cashOnAccount) {
            this.cashOnAccount = cashOnAccount;
        }

        @Override
        public double getBalance() {
            return cashOnAccount;
        }

        @Override
        public double withdrow(double amount) {
            if(cashOnAccount > amount){
                cashOnAccount -= amount;
                return cashOnAccount;
            }
            else
                return 0;
        }
    }
    static class TestCard implements  Card{
        private boolean isBlocked;
        private int pin;
        private Account account;

        TestCard(boolean isBlocked, int pin, Account account) {
            this.isBlocked = isBlocked;
            this.pin = pin;
            this.account = account;
        }

        @Override
        public boolean isBlocked() {
            return isBlocked;
        }

        @Override
        public Account getAccount() {
            return account;
        }

        @Override
        public boolean checkPin(int pinCode) {
            if(pin == pinCode){
                return true;
            }
            else
                return false;

        }
    }

    @Before
    public void setUp(){
        atm = new ATM(100000.0);
        emptyATM = new ATM(0);
        account = new TestAccount(1000);
        emptyAccount = new TestAccount(0);
        card = new TestCard(false, 1111, account);
        badCard1 = new TestCard(true, 1111, account);
        badCard2 = new TestCard(false, 1111, emptyAccount);

    }


    @Test
    public void testATMgetMoney(){
        assertEquals(atm.getMoneyInATM(), 100000.0, 0.0);
    }

    @Test
    public void testEmptyATMgetMoney(){
       assertEquals(emptyATM.getMoneyInATM(), 0, 0.0);
    }

    @Test
    public void testGoodCardValidation(){
       assertEquals(atm.validateCard(card, 1111), true);
    }
    @Test
    public void testBadCardValidation(){
        assertEquals(atm.validateCard(card, 2222), false);
    }
    @Test
    public void testInvalidCardValidation(){
        assertEquals(atm.validateCard(badCard1,1111), false);
    }

    @Test
    public void testCheckingValidCardCheckBalance() throws NoCardInsertedExpeption {
       atm.validateCard(card, 1111);
       assertEquals(atm.checkBalance(), 1000.0, 0.0);
    }

    @Test(expected=NoCardInsertedExpeption.class)
    public void testInvalidCardThrowsException() throws NoCardInsertedExpeption {
        atm.validateCard(card, 2222);
        atm.checkBalance();
    }

    @Test(expected=NoCardInsertedExpeption.class)
    public void testInvalidCardThrowsException2() throws NoCardInsertedExpeption {
        atm.validateCard(badCard1, 1111);
        atm.checkBalance();
    }

    @Test(expected=NotEnoughMoneyInATM.class)
    public void testEmptyATMGetCashThrowsException() throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
        emptyATM.validateCard(card, 1111);
        emptyATM.getCash(10.0);
    }

    @Test(expected=NotEnoughMoneyInAccount.class)
    public void testATMGetCashThrowsExceptionOnEmptyAccount() throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
        atm.validateCard(badCard2, 1111);
        atm.getCash(10.0);
    }

    @Test
    public void testATMGetCashWorksFineWithFineCard() throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
        atm.validateCard(card, 1111);
        atm.getCash(100.0);
        assertEquals(card.getAccount().getBalance(), 900.0, 0.0);
    }



}
