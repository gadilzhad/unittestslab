/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pti.myatm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author andrii
 * @author ezalor
 */

@RunWith(MockitoJUnitRunner.class)
public class ATMMockingTest {

    @Mock
    private static ATM atm;
    @Mock
    private static Card card;
    @Mock
    private static Account account;


    @Test
    public void testGetMoneyInATM() {;
        double expResult = 0.0;
        double result = atm.getMoneyInATM();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testValidateCard() {
        when(card.checkPin(0)).thenReturn(false);
        when(card.isBlocked()).thenReturn(true);

        int pinCode = 0;
        boolean expResult = false;
        boolean result = atm.validateCard(card, pinCode);
        assertEquals(expResult, result);

    }

    @Test
    public void testCheckBalance() {
            when(card.checkPin(1234)).thenReturn(true);
        when(card.isBlocked()).thenReturn(false);

        atm.validateCard(card, 1234);

        double expResult = 0.0;
        double result = 0;
        try {
            result = atm.checkBalance();
        } catch (NoCardInsertedExpeption noCardInsertedExpeption) {
           fail("It's not possible to fail that test");
        }
        assertEquals(expResult, result, 0.0);

    }

    @Test
    public void testGetCash() throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
        when(account.getBalance()).thenReturn(50.0);
        when(card.getAccount()).thenReturn(account);
        when(card.checkPin(1234)).thenReturn(true);
        when(card.isBlocked()).thenReturn(false);

        atm.validateCard(card, 1234);

        double amount = 25.0;

        double expResult = 25.0;
        double result = atm.getCash(amount);
        assertEquals(expResult, result, 25.0);

    }

}
